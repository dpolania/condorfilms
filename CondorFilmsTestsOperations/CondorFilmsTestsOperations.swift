//
//  CondorFilmsTestsOperations.swift
//  CondorFilmsTestsOperations
//
//  Created by Macbook  on 9/03/22.
//
import XCTest
import UIKit
import RapidUtil

class CondorFilmsTestsNavigation: XCTestCase {

    override func setUpWithError() throws {

    }

    override func tearDownWithError() throws {

    }

    func testHomePrincipal() throws {
        let navigation = NavigationMaps.principalFlowNavigation.homeViewController.getViewController()
        XCTAssertNotNil(navigation)
    }
    func testDetailPrincipal() throws {
        let navigation = NavigationMaps.principalFlowNavigation.detailViewController(MoviewDetail()).getViewController()
        XCTAssertNotNil(navigation)
    }
    func testPopUps() throws {
        let cotroller = FavoriteViewController()
        
        let navigation = NavigationMaps.PopUpsFlowNavigation.popUpFavorite(cotroller, "title", "description", true)
        XCTAssertNotNil(navigation)
    }
    
    
}
