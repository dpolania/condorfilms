//
//  CondorFilmsTests.swift
//  CondorFilmsTests
//
//  Created by Macbook  on 4/03/22.
//

import XCTest
import RxRelay
import RxSwift
import Lottie


@testable import CondorFilms

class CondorFilmsTestsGeneral: XCTestCase {
    var disposeBag = DisposeBag()
    override func setUpWithError() throws {
        
    }

    override func tearDownWithError() throws {
       
    }

    func testLottie() throws {
       let helperLottie = HelperLottie()
        let rating = 9
        let viewPercent = UIView()
        helperLottie.onAnimation?.asObservable().bind{ animation in
            let progress = (rating) / 10
            animation.play(fromProgress: 0,
                           toProgress: AnimationProgressTime(progress),
                                       loopMode: LottieLoopMode.playOnce,
                                       completion: { (finished) in
                                        if finished {
                                            XCTAssert(true)
                                        } else {
                                            XCTAssert(true)
                                        }
                    })
        }.disposed(by: disposeBag)
        helperLottie.animation?.accept(("rating", "Animations", viewPercent))
    }
    
    func testRealmHelperAdd(){
        let realm = RealmHelper()
        let data = dataForTestRealm()
        realm.add(data: data)
        let infoRealm = realm.allData(MoviewDetailteTest.self)
        XCTAssert(infoRealm.count > 0, "verificar si ingresa datos")
    }
    
    func testRealmHelperDelete(){
        let realm = RealmHelper()
        let data = dataForTestRealm()
        realm.add(data: data)
        realm.deleteAll(MoviewDetailteTest.self)
        let infoRealm = realm.allData(MoviewDetailteTest.self)
        XCTAssert(infoRealm.count == 0, "verificar borrado de datos")
    }
    
    func testRealmHelperSelected(){
        let realm = RealmHelper()
        realm.deleteAll(MoviewDetailteTest.self)
        let data = dataForTestRealm()
        realm.add(data: data)
        let infoRealm = realm.selectedData(MoviewDetailteTest.self, query: "id == 1")
        XCTAssert(infoRealm.count == 1, "verificar borrado de datos")
    }
    
    func testConstantsText(){
        let const =  ConstantString()
        let text = "\(const.returnString(key: .popUpCodeTitle, table: .details))"
        XCTAssertFalse(text.isEmpty)
    }
    
    func dataForTestRealm() -> MoviewDetailteTest{
        let data = MoviewDetailteTest()
        data.id = 1
        data.title = "prueba"
        return data
    }
}

