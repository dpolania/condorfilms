//
//  PopUpFavoriteViewController.swift
//  CondorFilms
//
//  Created by Macbook  on 8/03/22.
//

import UIKit
import RxSwift
import RxCocoa

class PopUpFavoriteViewController: UIViewController {
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var labelDetail: UILabel!{
        didSet{
            labelDetail.text = descriptionPopUps
        }
    }
    @IBOutlet weak var labelTitle: UILabel!{
        didSet{
            labelTitle.text = titlePopUps
        }
    }
    
     var titlePopUps: String = ""
     var descriptionPopUps: String = ""
    var isOneAction : Bool = false
    
    var popUps : IPopUps? = nil
    var disposeBag = DisposeBag()
        
    override func viewDidLoad() {
                super.viewDidLoad()
        buttonCancel.isHidden = isOneAction
        rxSetup()
    }
    
    func rxSetup(){
        buttonDone.rx.tap.bind{
            self.popUps?.actionDone(isOneAction: self.isOneAction)
        }.disposed(by: disposeBag)
        
        buttonCancel.rx.tap.bind{
            self.popUps?.actionCancel()
        }.disposed(by: disposeBag)
    }
}
