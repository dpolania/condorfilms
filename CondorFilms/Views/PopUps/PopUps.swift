//
//  PopUps.swift
//  CondorFilms
//
//  Created by Macbook  on 8/03/22.
//

import Foundation
import UIKit
import RapidUtil

protocol IPopUpsPresenter{
    func dismissPopUps(viewcont : UIViewController)
    func presentPopUps(view: UIViewController,popUps:Flow)
}

protocol IPopUps{
    func actionDone(isOneAction:Bool)
    func actionCancel()
}

class PopUpsPresenter: IPopUpsPresenter {
    func dismissPopUps(viewcont : UIViewController) {
        let view = viewcont.view.viewWithTag(999)
        view?.removeFromSuperview()
    }
    
    func presentPopUps(view: UIViewController,popUps:Flow) {
        let controller = popUps.getViewController()
        controller.modalPresentationStyle = .overFullScreen
        controller.view.tag = 999
        controller.view.frame = view.view.frame
        view.view.addSubview(controller.view)
    }
    
    
}
