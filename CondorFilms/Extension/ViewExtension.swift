//
//  ViewExtension.swift
//  CondorFilms
//
//  Created by Macbook  on 7/03/22.
//
import UIKit

extension UIView {
    @IBInspectable
    public var withShadow:Bool {
        get {
            return self.withShadow
        }
        set {
            if newValue {
                setShadow()
            }
        }
    }
    struct Corners {
        static var topLeft:Bool = false
        static var topRight:Bool = false
        static var bottomLeft:Bool = false
        static var bottomRight:Bool = false
    }
    @IBInspectable
    public var cornerRadius:CGFloat {
        set (radius) {
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = radius > 0
        }
        get {
            return self.layer.cornerRadius
        }
    }
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return .clear
        }
        set {
            layer.borderColor = newValue.cgColor
        }
    }
    @IBInspectable
    public var roundTopLeft:Bool {
        set {
            Corners.topLeft = newValue
            self.roundCorners(topLeft: newValue, topRight: roundTopRight, bottomLeft: roundBottomLeft, bottomRight: roundBottomRight)
        }
        get {
            return Corners.topLeft
        }
    }
    @IBInspectable
    public var roundTopRight:Bool {
        set {
            Corners.topRight = newValue
            self.roundCorners(topLeft: roundTopLeft, topRight: newValue, bottomLeft: roundBottomLeft, bottomRight: roundBottomRight)
        }
        get {
            return Corners.topRight
        }
    }
    @IBInspectable
    public var roundBottomLeft:Bool {
        set {
            Corners.bottomLeft = newValue
            self.roundCorners(topLeft: roundTopLeft, topRight: roundTopRight, bottomLeft: newValue, bottomRight: roundBottomRight)
        }
        get {
            return Corners.bottomLeft
        }
    }
    @IBInspectable
    public var roundBottomRight:Bool {
        set {
            Corners.bottomRight = newValue
            self.roundCorners(topLeft: roundTopLeft, topRight: roundTopRight, bottomLeft: roundBottomLeft, bottomRight: newValue)
        }
        get {
            return Corners.bottomRight
        }
    }
    
    func roundCorners(topLeft:Bool, topRight:Bool, bottomLeft:Bool, bottomRight:Bool) {
        var corners:CACornerMask = []
        if topLeft {
            corners.insert(.layerMinXMinYCorner)
        }
        if topRight {
            corners.insert(.layerMaxXMinYCorner)
        }
        if bottomLeft {
            corners.insert(.layerMinXMaxYCorner)
        }
        if bottomRight {
            corners.insert(.layerMaxXMaxYCorner)
        }
        roundCorners(corners: corners)
    }
    
    func roundCorners(corners: CACornerMask) {
        self.clipsToBounds = true
        self.layer.maskedCorners = corners
    }
    
    func addShadow(offset: CGSize, color: UIColor = .lightGray, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    func setShadow(_ color:UIColor = .lightGray, opacity: Float = 1.0) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = .zero
        layer.shadowOpacity = opacity
        layer.shadowRadius = 1.0
        clipsToBounds = false
        layer.masksToBounds = false
    }
    
    
    //MARK: Contraint variables
    
    /// Variable for change constant values from height anchor contraint
    var heightConstraint: NSLayoutConstraint? {
        get {
            return constraints.first { $0.firstAttribute == .height && $0.relation == .equal }
        }
        set { setNeedsLayout() }
    }
    
    
    
    func setLayerRadius(radius: CGFloat, edges: CACornerMask) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = edges
    }
    
       
       @IBInspectable
       var bordeColor: UIColor? {
           get {
               if let color = layer.borderColor {
                   return UIColor(cgColor: color)
               }
               return nil
           }
           set {
               if let color = newValue {
                   layer.borderColor = color.cgColor
               } else {
                   layer.borderColor = nil
               }
           }
       }
}

//NARK: borders View
extension UIView {
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}
