//
//  PrincipalFlowNavigation.swift
//  condorFilms
//
//  Created by David Camilo Polania Losada on 4/03/22.
//

import UIKit
import RapidUtil

extension NavigationMaps{
    /**
        Este enumerador contiene el flujo basico de la aplicacion es el encargado
        de agrupar aquellas vista que tiene un flujo o secuencia en comun
     */
    enum principalFlowNavigation : Flow{
        case splashViewController
        case homeViewController
        case detailViewController(MoviewDetail)
        case favoriteViewController
        
        func getViewController() -> UIViewController {
            switch self {
            
            case .splashViewController:
                return navigateToSplash()
            case .homeViewController:
                return navigationToHome()
            case .detailViewController(let data):
                return navigateToDetail(data)
            case .favoriteViewController:
                return navigateToFavorite()
            }
        }
        
        func navigateToSplash() -> UIViewController{
            let controller = SplashViewController()
            controller.router = SplashRoute(controller)
            controller.lottieHelper = HelperLottie()
            return controller
        }
        
        func navigationToHome() -> UIViewController{
            let controller = HomeViewController()
            let router = HomeRouter(view: controller)
            let manager = HomeManager()
            let viewModel = HomeViewModel(manager: manager)
            
            controller.viewModel = viewModel
            controller.router = router
            controller.const = ConstantString()
            controller.popUpsPresenter = PopUpsPresenter()
            return controller
        }
        
        func navigateToDetail(_ data : MoviewDetail) -> UIViewController{
            let control = DetailViewController()
            control.moviewDetail = data
            let manager = DetailManager()
            control.viewModel = DetailViewModel(manager: manager)
            control.popUpsPresenter = PopUpsPresenter()
            control.const = ConstantString()
            return control
        }
        
        func navigateToFavorite()->UIViewController{
            let control = FavoriteViewController()
            control.viewModel = FavoriteViewModel()
            control.router = FavoriteRouter(view: control)
            control.popUpsPresenter = PopUpsPresenter()
            control.const = ConstantString()
            return control
        }
    }
}
