//
//  ProtocolsNavigation.swift
//  condorFilms
//
//  Created by Macbook  on 4/03/22.
//

import UIKit

protocol IRouter {
    var  view : UIViewController? { get set }
}
