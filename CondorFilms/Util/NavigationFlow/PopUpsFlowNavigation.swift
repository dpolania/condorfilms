//
//  PopUpsFlowNavigation.swift
//  CondorFilms
//
//  Created by Macbook  on 8/03/22.
//

import Foundation
import UIKit
import RapidUtil

extension NavigationMaps{
    /**
        Enumerador que contiene los popUps que usa la aplicacion
     */
    enum PopUpsFlowNavigation : Flow{
        case popUpFavorite(IPopUps,String,String,Bool)
        
        func getViewController() -> UIViewController {
            switch self {
            
            case .popUpFavorite(let view,let title,let description,let isOneAction):
                return navigatePopUpFavorite(delegate: view,title: title,description: description,isOneAction)
            }
        }
        
        func navigatePopUpFavorite(delegate : IPopUps,title:String, description: String,_ isOneAction : Bool = false) -> UIViewController{
            let controller = PopUpFavoriteViewController()
            controller.popUps = delegate
            controller.titlePopUps = title
            controller.descriptionPopUps = description
            controller.isOneAction = isOneAction
            return controller
        }
    }
}
