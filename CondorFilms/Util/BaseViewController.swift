//
//  BaseViewController.swift
//  CondorFilms
//
//  Created by Macbook  on 5/03/22.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    var isHiddenNavigation : Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        visivilityNavigationBar(isHidden: isHiddenNavigation, animated: animated)
    }
    
    func visivilityNavigationBar(isHidden:Bool,animated : Bool){
        navigationController?.setNavigationBarHidden(isHidden, animated: animated)
    }
    /**
        Muestra el loadin de la aplicacion
     */
    func showLoading(){
        let contentView = UIView()
        let indicator = UIActivityIndicatorView()
        contentView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        contentView.center = view.center
        contentView.backgroundColor = UIColor.clear
        contentView.clipsToBounds = true
        contentView.layer.cornerRadius = 10

        indicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        indicator.style = UIActivityIndicatorView.Style.large
        indicator.center = CGPoint(x: contentView.bounds.width / 2, y: contentView.bounds.height / 2)

        contentView.addSubview(indicator)
        contentView.tag = 998
        view.addSubview(contentView)

        indicator.startAnimating()
    }
    /**Quita el loadin de la aplicación*/
    func hideLoading(){
        let view = self.view.viewWithTag(998)
        view?.removeFromSuperview()
    }
}

