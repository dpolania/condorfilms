//
//  HelperLottie.swift
//  CondorFilms
//
//  Created by Macbook  on 5/03/22.
//

import Foundation
import RxSwift
import RxRelay
import RxCocoa
import Lottie

protocol IHelperLottie {
    var animation : PublishRelay<(String,String,UIView)>? { get set }
    var onAnimation : Driver<AnimationView>? { get set }
}
/**
 utilidad que nos permite parametrizar el contenedor de la animacion
 que va ha ser incertada en la vista seleecionada
 */
class HelperLottie : IHelperLottie {
    var animation: PublishRelay<(String, String,UIView)>?
    var onAnimation : Driver<AnimationView>?
    var disposeBag = DisposeBag()
    
    init(){
        self.animation = PublishRelay<(String, String,UIView)>()
        initAnimation()
    }
    /**
     Inicializa el contenedor con los parametros basicos para cargar la animacion
     y la vista donde se insertara esta funcion transforma el PublishRelay en un
     Observable del tipo de AnimationView
     */
    func initAnimation(){
        self.onAnimation  = self.animation?.flatMapLatest{ (archive,rute,vwAnimation) -> Observable<AnimationView> in
            return Observable.create{ (observer) -> Disposable in
                guard let animation = Animation.named(archive, subdirectory: rute)
                else {
                    observer.onError(ErrorGeneric.noFound)
                    return Disposables.create()
                }
                let animationView = AnimationView()
                animationView.animation = animation
                animationView.contentMode = .scaleAspectFit
                
                animationView.contentMode = .scaleAspectFit
                vwAnimation.addSubview(animationView)
                animationView.translatesAutoresizingMaskIntoConstraints = false
                animationView.topAnchor.constraint(equalTo: vwAnimation.topAnchor).isActive = true
                animationView.leadingAnchor.constraint(equalTo: vwAnimation.leadingAnchor).isActive = true
                
                animationView.bottomAnchor.constraint(equalTo: vwAnimation.bottomAnchor, constant: 0).isActive = true
                animationView.trailingAnchor.constraint(equalTo: vwAnimation.trailingAnchor).isActive = true
                animationView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
                
                observer.onNext(animationView)
                return Disposables.create()
            }
        }.asDriver(onErrorRecover: { Error in
            return Driver.just(AnimationView())
        })
        
       _ =  self.animation?.subscribe(onNext:{ animation,route,view in
            print("action init animation: \(animation) - \(route) - \(view.description)")
       }).disposed(by: disposeBag)
        
        
    }
}
