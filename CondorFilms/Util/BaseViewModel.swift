//
//  BaseViewModel.swift
//  CondorFilms
//
//  Created by Macbook  on 4/03/22.
//
import RxSwift
import RxCocoa

struct Output <T> {
    let response:Driver<T>
    let errorMessage:Driver<String>
}

protocol IBaseViewModel {
    
}

class BaseViewModel : IBaseViewModel {
    
}
enum ErrorGeneric : Error{
    case noFound
}
