//
//  ApiHelper.swift
//  CondorFilms
//
//  Created by Macbook  on 5/03/22.
//

import Foundation

/**
 Parametros para el uso del apiRest
 */
struct ApiParams {
    static let baseURL = "https://api.themoviedb.org/3/"
    static let apiKey = "ac7fe369b98c8c82f1b15787fb335ae6"
    static var urlImage = "https://image.tmdb.org/t/p/w500"
}

/**Metodos del servicio apiRest*/
struct ApiServices {
    static let popularmovie   = "\(ApiParams.baseURL)movie/popular?api_key=\(ApiParams.apiKey)&language=es-ES&page=1"
    static var detailMovie : (Int)->String = {id in
            return "\(ApiParams.baseURL)movie/\(id)?api_key=\(ApiParams.apiKey)&language=en-ES"
    }
    
    static let trailers : (Int)->String = {id in
        return "\(ApiParams.baseURL)movie/\(id)/videos?api_key=\(ApiParams.apiKey)&language=en-ES"
}
}
