//
//  DetailManager.swift
//  CondorFilms
//
//  Created by Macbook  on 6/03/22.
//

import RxSwift
import RxRelay
import RapidUtil

protocol IDetailManager {
    func requestTrailer(id : Int)->Observable<[ResultTrailer]>
    var errorReques : PublishRelay<Error>? {get set}
}

class DetailManager: IDetailManager {
    let network = networUtil()
    var errorReques : PublishRelay<Error>?
    
    init() {
        errorReques =  PublishRelay<Error>()
    }
    
    func requestTrailer(id : Int) -> Observable<[ResultTrailer]> {
        return Observable.create { (observer) -> Disposable in
            self.network.requestAPI(url: ApiServices.trailers(id),method: .get){completion,error in
                guard error == nil else {
                    print("Ocurrio un error \(error.debugDescription)")
                    self.errorReques?.accept(error!)
                    return
                }

                guard let list = completion?.deserialize(type: TrailersModel.self) else {
                    self.errorReques?.accept(ManagerError.noFound)
                    return
                }
                observer.onNext(list.results ?? [])
            }
            return Disposables.create()
        }
    }
    
    // do someting...
}
