//
//  DetailViewModel.swift
//  CondorFilms
//
//  Created by Macbook  on 6/03/22.
//

import Foundation
import RxSwift
import RxCocoa

protocol IDetailViewModel {
    func addFavorite(to item: MoviewDetail, action:  @escaping  ((ResultOperation)->Void))
    var realm : RealmHelper? { get set }
    var output : Output<[ResultTrailer]>? { get set }
    var treileRequest: PublishRelay<Int> { get set }
    var manager: IDetailManager {get set}
}

class DetailViewModel: IDetailViewModel {
    
    var realm: RealmHelper?
    var output : Output<[ResultTrailer]>?
    var treileRequest: PublishRelay<Int>
    var manager: IDetailManager
    
    init(manager: IDetailManager) {
        self.manager = manager
        treileRequest = PublishRelay<Int>()
        realm = RealmHelper()
        wachTrailer()
    }
    func addFavorite(to item: MoviewDetail, action:  @escaping  ((ResultOperation)->Void)) {
        guard (self.realm?.selectedData(MoviewDetailRealm.self, query: "id == \(item.id ?? 0)").first) == nil
        else{
            action(.exist)
            return
        }
        
        self.realm?.add(data: item.objectRealm())
        action(.success)
        print("all info: \n \(String(describing: self.realm?.allData(MoviewDetailRealm.self)))")
    }
    func wachTrailer(){
        let request = treileRequest.flatMapLatest{  id-> Observable<[ResultTrailer]> in
            return self.manager.requestTrailer(id: id)
        }.asDriver(onErrorRecover: { Error in
            return Driver.just([ResultTrailer()])
        })
        let errorDriver = PublishSubject<String>().asDriver(onErrorJustReturn: "ERROR")
        
        self.output = Output<[ResultTrailer]>(response: request, errorMessage: errorDriver)
    }
}
enum ResultOperation {
    case success
    case error
    case exist
}
