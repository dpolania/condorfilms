//
//  DetailModel.swift
//  CondorFilms
//
//  Created by Macbook  on 9/03/22.
//

import Foundation

struct TrailersModel : Codable{
    var id : Int?
    var results : [ResultTrailer]?
}

struct ResultTrailer : Codable{
    var iso_639_1 : String?
    var iso_3166_1: String?
    var name : String?
    var key : String?
    var site : String?
    var size : Int?
    var type : String?
    var official : Bool?
    var published_at : String?
    var id : String?
}
