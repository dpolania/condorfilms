//
//  DetailRouter.swift
//  CondorFilms
//
//  Created by Macbook  on 6/03/22.
//

import UIKit

protocol IDetailRouter  : IRouter{
    func navigationHome()
}

class DetailRouter: IDetailRouter {
    var view: UIViewController?
    init(view:UIViewController) {
        self.view = view
    }
    func navigationHome() {
        //TODO
    }
}
