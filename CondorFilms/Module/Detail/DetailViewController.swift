//
//  DetailViewController.swift
//  CondorFilms
//
//  Created by Macbook  on 6/03/22.
//

import UIKit
import RxSwift
import RxCocoa
import RapidUtil

protocol IDetailViewController {
    var moviewDetail : MoviewDetail? { get set }
    var viewModel : IDetailViewModel? { get set }
    var popUpsPresenter : IPopUpsPresenter? {get set}
    var const :  ConstantString? {get set}
}

class DetailViewController: BaseViewController,IDetailViewController {

    @IBOutlet weak var buttonYoutube: UIButton!
    @IBOutlet weak var butttonFavorite: UIButton!
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var imageBackdrop: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelReleaseDate: UILabel!
    @IBOutlet weak var labelBudget: UILabel!
    @IBOutlet weak var textViewOverview: UITextView!
    @IBOutlet weak var visualEffectBackground: UIVisualEffectView!
    
    var disposeBag = DisposeBag()
    var moviewDetail : MoviewDetail?
    var viewModel : IDetailViewModel?
    var popUpsPresenter : IPopUpsPresenter?
    var const :  ConstantString?
    var idYoutube : String?{
        didSet{
            if !(idYoutube?.isEmpty ?? true){
                buttonYoutube.isHidden = false
            }
        }
    }
     
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonYoutube.isHidden = true
        self.rxSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupView()
        viewModel?.treileRequest.accept(moviewDetail?.id ?? 0)
    }
    func setupView(){
        let format = FormatCurrency()
        self.labelTitle.text = moviewDetail?.title ?? ""
        self.labelBudget.text = format.convertNumberCurrency(number: Double(moviewDetail?.budget ?? 0))
        self.labelReleaseDate.text = moviewDetail?.release_date ?? ""
        self.textViewOverview.text = moviewDetail?.overview ?? ""
        
        self.imageBackdrop.downloaded(from: "\(ApiParams.urlImage)\(moviewDetail?.backdrop_path ?? "")")
        self.imageBackdrop.contentMode = .scaleAspectFill
    }
    
    func rxSetup(){
        self.buttonClose.rx.tap.bind{
            self.dismiss(animated: true, completion: nil)
        }.disposed(by: disposeBag)
        
        viewModel?.output?.response.asObservable().bind{ result in
            self.idYoutube = result.first?.key
        }.disposed(by: disposeBag)
        
        self.butttonFavorite.rx.tap.bind{
            let title = self.const?.returnString(key: .popUpsTitle, table: .details) ?? ""
            let description = self.const?.returnString(key: .popDescription, table: .details) ?? ""
            self.popUpsPresenter?.presentPopUps(view: self, popUps:NavigationMaps.PopUpsFlowNavigation.popUpFavorite(self,  title, description,false))
        }.disposed(by: disposeBag)
        
        self.buttonYoutube.rx.tap.bind{
            let youtubeId = self.idYoutube ?? ""
            var youtubeUrl = URL(string:"youtube://\(String(describing: youtubeId))")!
            if UIApplication.shared.canOpenURL(youtubeUrl ){
                UIApplication.shared.open(youtubeUrl, options: [:], completionHandler: nil)
                } else{
                    youtubeUrl = URL(string:"https://www.youtube.com/watch?v=\(String(describing: youtubeId))")!
                    UIApplication.shared.open(youtubeUrl, options: [:], completionHandler: nil)
                }
        }.disposed(by: disposeBag)
    }
}

extension DetailViewController:IPopUps{
    func actionDone(isOneAction: Bool) {
        if !isOneAction {
            self.viewModel?.addFavorite(to: self.moviewDetail ?? MoviewDetail()){ action in
                switch action{
                case .exist:
                    let title = self.const?.returnString(key: .popUpsTitleWarnning, table: .details) ?? ""
                    let description = self.const?.returnString(key: .popUpsExistData, table: .details) ?? ""
                    self.popUpsPresenter?.presentPopUps(view: self, popUps:NavigationMaps.PopUpsFlowNavigation.popUpFavorite(self,  title, description,true))
                case .success :
                    self.dismiss(animated: true, completion: nil)
                default :
                    let title = self.const?.returnString(key: .popUpTitleRealm, table: .details) ?? ""
                    let description = self.const?.returnString(key: .popUpDetailRealm, table: .details) ?? ""
                    self.popUpsPresenter?.presentPopUps(view: self, popUps:NavigationMaps.PopUpsFlowNavigation.popUpFavorite(self,  title, description,true))
                }
                
                if action == .exist {
                    let title = self.const?.returnString(key: .popUpsTitleWarnning, table: .details) ?? ""
                    let description = self.const?.returnString(key: .popUpsExistData, table: .details) ?? ""
                    self.popUpsPresenter?.presentPopUps(view: self, popUps:NavigationMaps.PopUpsFlowNavigation.popUpFavorite(self,  title, description,true))
                }
            }
        }
        popUpsPresenter?.dismissPopUps(viewcont: self)
    }
    
    func actionCancel() {
        popUpsPresenter?.dismissPopUps(viewcont: self)
    }
}
