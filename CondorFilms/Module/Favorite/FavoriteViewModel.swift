//
//  FavoriteViewModel.swift
//  CondorFilms
//
//  Created by Macbook  on 8/03/22.
//

import Foundation
import RxSwift
import RxRelay
import RealmSwift

protocol IFavoriteViewModel{
    func getAllFavorite()
    var realm : RealmHelper? { get set }
    var data : BehaviorRelay<[MoviewDetail]>? {get set}
    var resultAction : PublishRelay<ResultOperation>? {get set}
    var dataRemove : BehaviorRelay<MoviewDetail>? {get set}
    var popUpsPresenter : IPopUpsPresenter? {get set}
    var errorRequest : PublishRelay<Error>{get set}
}
class FavoriteViewModel : IFavoriteViewModel{
    var data: BehaviorRelay<[MoviewDetail]>?
    var dataRemove : BehaviorRelay<MoviewDetail>?
    var resultAction : PublishRelay<ResultOperation>?
    var popUpsPresenter : IPopUpsPresenter?
    var realm: RealmHelper?
    var disposeBag = DisposeBag()
    var errorRequest : PublishRelay<Error>
    
    init() {
        data =  BehaviorRelay<[MoviewDetail]>(value: [MoviewDetail]())
        dataRemove = BehaviorRelay<MoviewDetail>(value: MoviewDetail())
        resultAction = PublishRelay<ResultOperation>()
        realm = RealmHelper()
        errorRequest  = PublishRelay<Error>()
        getAllFavorite()
        deleteItem()
    }
    func deleteItem(){
        dataRemove?.asObservable().subscribe(onNext:{object in
            if !(object.title?.isEmpty ?? true) {
                if let remove = self.selectListRealm()?.first(where: {
                    $0.id == object.id
                }){
                    self.realm?.delete(data: remove,action: { actionResult in
                        self.resultAction?.accept(actionResult)
                    })
                    self.getAllFavorite()
                }
            }
        }).disposed(by: disposeBag)
    }
    func getAllFavorite() {
        let result = realm?.allData(MoviewDetailRealm.self)
        var movieFavorite = [MoviewDetail]()
        result?.forEach({
           let item =  MoviewDetail(id: $0.id,
                                title: $0.title,
                                budget: $0.budget,
                                backdrop_path: $0.backdrop_path,
                                overview: $0.overview,
                                release_date: $0.release_date,
                                poster_path: $0.poster_path,
                                isFavorite: $0.isFavorite)
                                movieFavorite.append(item)
        })
        data?.accept(movieFavorite)
    }
    private func selectListRealm()-> Results<MoviewDetailRealm>?{
        let result = realm?.allData(MoviewDetailRealm.self)
        return result
    }
}
