//
//  FavoriteTableViewCell.swift
//  CondorFilms
//
//  Created by Macbook  on 8/03/22.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var buttonDelete: UIButton!
    @IBOutlet weak var imagePoster: UIImageView!
    
    var onDeleted : (()->Void?)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        rxSetup()
        // Configure the view for the selected state
    }
    
    func rxSetup(){
        buttonDelete.rx.tap.bind{
            (self.onDeleted ?? {
                print("no custom action")
            })()
        }
    }
    static func getNib () -> UINib {
        return UINib(nibName: "FavoriteTableViewCell", bundle: nil)
    }
}
