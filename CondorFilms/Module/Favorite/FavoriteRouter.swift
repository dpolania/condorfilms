//
//  FavoriteRouter.swift
//  CondorFilms
//
//  Created by Macbook  on 8/03/22.
//

import Foundation
import UIKit
import RapidUtil

protocol IFavoriteRouter  : IRouter{
    func navigationToDetail(detail : MoviewDetail)
}

class FavoriteRouter: IFavoriteRouter {
    var view: UIViewController?
    init(view:UIViewController) {
        self.view = view
    }
    func navigationToDetail(detail : MoviewDetail) {
        self.view?.presentView(NavigationMaps.principalFlowNavigation.detailViewController(detail), styleModal: .overFullScreen)
    }
}
