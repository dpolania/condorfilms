//
//  FavoriteViewController.swift
//  CondorFilms
//
//  Created by Macbook  on 8/03/22.
//

import UIKit
import RxSwift
import RxCocoa
import RapidUtil

protocol IFavoriteViewController{
    var viewModel: IFavoriteViewModel? { get set }
    var router : IFavoriteRouter? {get set}
    var const : ConstantString?{get set}
    var popUpsPresenter : IPopUpsPresenter? {get set}
}
class FavoriteViewController: BaseViewController,IFavoriteViewController  {
    
    @IBOutlet weak var tableFavorite: UITableView!
    
    var viewModel: IFavoriteViewModel?
    var router : IFavoriteRouter?
    var disposeBag = DisposeBag()
    var popUpsPresenter : IPopUpsPresenter?
    var const : ConstantString?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        rxSetup()
    }
    func setupViews() {
            overrideUserInterfaceStyle = .light
        self.tableFavorite.register(FavoriteTableViewCell.getNib(), forCellReuseIdentifier: "FavoriteTableViewCell")
        }
    
    func rxSetup(){
        viewModel?.errorRequest.asObservable().bind{ error in
            DispatchQueue.main.async {
                self.hideLoading()
                let err = error as NSError
                
                let title = "\(self.const?.returnString(key: .popUpCodeTitle, table: .details) ?? "" )\(err.code)"
                let description = self.const?.returnString(key: .popUpsHttpError, table: .details) ?? ""
                self.popUpsPresenter?.presentPopUps(view: self, popUps:NavigationMaps.PopUpsFlowNavigation.popUpFavorite(self,  title, description,true))
            }
            
        }.disposed(by: disposeBag)
        
        viewModel?.data?.asObservable().bind(to: tableFavorite.rx.items(cellIdentifier: "FavoriteTableViewCell",cellType: FavoriteTableViewCell.self)){index, element, cell in
            cell.labelTitle.text = element.title
            cell.labelDate.text = element.release_date
            cell.imagePoster.downloaded(from: "\(ApiParams.urlImage)\(element.poster_path ?? "")")
            cell.onDeleted = {
                self.viewModel?.dataRemove?.accept(element)
            }
        }.disposed(by: disposeBag)
        
        viewModel?.resultAction?.subscribe(onNext:{ action in
            if action == .error {
                let title = self.const?.returnString(key: .popUpTitleRealm, table: .details) ?? ""
                let description = self.const?.returnString(key: .popUpDetailRealm, table: .details) ?? ""
                self.popUpsPresenter?.presentPopUps(view: self, popUps:NavigationMaps.PopUpsFlowNavigation.popUpFavorite(self,  title, description,true))
            }
        }).disposed(by: disposeBag)
        
        tableFavorite
                .rx
                .modelSelected(MoviewDetail.self)
                .subscribe(onNext: { (selected) in
                    self.router?.navigationToDetail(detail: selected)
                }).disposed(by: disposeBag)
    }
}

extension FavoriteViewController : IPopUps{
    func actionDone(isOneAction: Bool) {
        print("actionDone")
        popUpsPresenter?.dismissPopUps(viewcont: self)
    }
    
    func actionCancel() {
        print("actionDone")
    }
    
    
}
