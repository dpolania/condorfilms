//
//  HomeModel.swift
//  CondorFilms
//
//  Created by Macbook  on 4/03/22.
//

import Foundation
import UIKit
import RealmSwift

struct ResultMovie : Codable{
    var id:Int?
    var title:String?
    var vote_average : Double?
    var poster_path : String?
}

struct MoviesList : Codable{
    var page:Int?
    var results : [ResultMovie]?
}

struct MoviewDetail : Codable{
    var id:Int?
    var title : String?
    var budget :  Int?
    var backdrop_path : String?
    var overview : String?
    var release_date : String?
    var poster_path : String?
    var isFavorite : Bool? = false
    
    func objectRealm() -> Object{
        let object = MoviewDetailRealm()
        object.id = self.id ?? 0
        object.title =  self.title
        object.budget =  self.budget ?? 0
        object.backdrop_path =  self.backdrop_path
        object.overview =  self.overview
        object.release_date =  self.release_date
        object.poster_path = self.poster_path
        object.isFavorite = true
        return object
    }
}

class MoviewDetailRealm : Object{
    @objc dynamic  var id: Int = 0
    @objc dynamic var title : String?
    @objc dynamic var budget :  Int = 0
    @objc dynamic var backdrop_path : String?
    @objc dynamic var overview : String?
    @objc dynamic var release_date : String?
    @objc dynamic var poster_path : String?
    @objc dynamic var isFavorite : Bool = false
}
