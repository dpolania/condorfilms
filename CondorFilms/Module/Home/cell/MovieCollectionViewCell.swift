//
//  MovieCollectionViewCell.swift
//  CondorFilms
//
//  Created by Macbook  on 5/03/22.
//

import UIKit
import Lottie
import RxSwift

class MovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imagePoster: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewPercent: UIView!
    @IBOutlet weak var labelRating: UILabel!
    
    var helperLottie =  HelperLottie()
    var disposeBag = DisposeBag()
    var rating : CGFloat? {
        didSet{
            labelRating.text = "\(String(describing: rating ?? 0))"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    override func didMoveToSuperview() {
        setupRx()
    }
    
    func setupRx(){
        helperLottie.onAnimation?.asObservable().bind{ animation in
            let progress = (self.rating ?? CGFloat(0)) / 10
            animation.play(fromProgress: 0,
                           toProgress: progress,
                                       loopMode: LottieLoopMode.playOnce,
                                       completion: { (finished) in
                                        if finished {
                                            print("Animation Finish")
                                        } else {
                                            print("Animation cancelled")
                                        }
                    })
        }.disposed(by: disposeBag)
        helperLottie.animation?.accept(("rating", "Animations", viewPercent))
    }
    
    static func getNib () -> UINib {
        return UINib(nibName: "MovieCollectionViewCell", bundle: nil)
    }
}
