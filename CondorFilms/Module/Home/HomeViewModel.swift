//
//  HomeViewModel.swift
//  CondorFilms
//
//  Created by Macbook  on 4/03/22.
//

import Foundation
import RxSwift
import RxCocoa

protocol IHomeViewModel {
    var movieRequest:  PublishRelay<Void> { get set }
    var output : Output<[ResultMovie]>? { get set }
    
    var movieDetailRequest:  PublishRelay<Int> { get set }
    var outputDetail : Output<MoviewDetail>? { get set }
    
    var errorRequest : PublishRelay<Error>{get set}
}

class HomeViewModel: IHomeViewModel {
    var movieDetailRequest: PublishRelay<Int>
    var outputDetail: Output<MoviewDetail>?
    
    var output: Output<[ResultMovie]>?
    var manager: IHomeManager
    var movieRequest: PublishRelay<Void>
    var errorRequest : PublishRelay<Error>
    var disposeBag = DisposeBag()
    
    init(manager: IHomeManager) {
        self.manager = manager
        errorRequest  = PublishRelay<Error>()
        self.movieRequest = PublishRelay<Void>()
        self.movieDetailRequest  = PublishRelay<Int>()
        
        setupError()
        configureMovie()
        configureMovieDetail()
    }
    
    func configureMovie(){
        let request = movieRequest.flatMapLatest{  _ -> Observable<[ResultMovie]> in
            return self.manager.requestMovies()
        }.asDriver(onErrorRecover: { Error in
            return Driver.just([ResultMovie()])
        })
        let errorDriver = PublishSubject<String>().asDriver(onErrorJustReturn: "ERROR")
        
        self.output = Output<[ResultMovie]>(response: request, errorMessage: errorDriver)
    }
    func configureMovieDetail(){
        let request = movieDetailRequest.flatMapLatest{  id -> Observable<MoviewDetail> in
            return self.manager.requestMoviesDetail(id: id)
        }.asDriver(onErrorRecover: { Error in
            return Driver.just(MoviewDetail())
        })
        let errorDriver = PublishSubject<String>().asDriver(onErrorJustReturn: "ERROR")
        
        self.outputDetail = Output<MoviewDetail>(response: request, errorMessage: errorDriver)
    }
    func setupError(){

        self.manager.errorReques?.asObservable().bind{ error in
            self.errorRequest.accept(error)
        }.disposed(by: disposeBag)
    }
}
