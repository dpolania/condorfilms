//
//  HomeRouter.swift
//  CondorFilms
//
//  Created by Macbook  on 4/03/22.
//

import UIKit
import RapidUtil

protocol IHomeRouter  : IRouter{
    func navigationToDetail(detail : MoviewDetail)
    func navigationToFavoritel()
}

class HomeRouter: IHomeRouter {
    var view: UIViewController?
    init(view:UIViewController) {
        self.view = view
    }
    func navigationToDetail(detail : MoviewDetail) {
        self.view?.presentView(NavigationMaps.principalFlowNavigation.detailViewController(detail), styleModal: .overFullScreen)
    }
    func navigationToFavoritel() {
        self.view?.presentView(NavigationMaps.principalFlowNavigation.favoriteViewController, styleModal: .popover)
    }
}
