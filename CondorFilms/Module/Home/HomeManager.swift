//
//  HomeManager.swift
//  CondorFilms
//
//  Created by Macbook  on 4/03/22.
//
import RxSwift
import RapidUtil
import RxRelay

protocol IHomeManager {
    func requestMovies()->Observable<[ResultMovie]>
    func  requestMoviesDetail(id : Int) -> Observable<MoviewDetail>
    var errorReques : PublishRelay<Error>? {get set}
}

class HomeManager: IHomeManager {
    let network = networUtil()
    var errorReques : PublishRelay<Error>?
    
    init() {
        errorReques =  PublishRelay<Error>()
    }
    
    func requestMovies() -> Observable<[ResultMovie]> {
        return Observable.create { (observer) -> Disposable in
            self.network.requestAPI(url: ApiServices.popularmovie,method: .get){completion,error in
                guard error == nil else {
                    print("Ocurrio un error \(error.debugDescription)")
                    self.errorReques?.accept(error!)
                    return
                }

                guard let list = completion?.deserialize(type: MoviesList.self) else {
                    self.errorReques?.accept(ManagerError.noFound)
                    return
                }
                observer.onNext(list.results ?? [])
            }
            return Disposables.create()
        }
    }
    
    func requestMoviesDetail(id : Int) -> Observable<MoviewDetail> {
            return Observable.create { (observer) -> Disposable in
                self.network.requestAPI(url: ApiServices.detailMovie(id),method: .get){completion,error in
                    guard error == nil else {
                        print("Ocurrio un error \(error.debugDescription)")
                        self.errorReques?.accept(error!)
                        return
                    }

                    guard let data = completion?.deserialize(type: MoviewDetail.self) else {
                        self.errorReques?.accept(ManagerError.noFound)
                        return
                    }
                    observer.onNext(data)
                }
                return Disposables.create()
            }
        }
    
    // do someting...
}

enum ManagerError : Error{
    case noFound
}
