//
//  HomeViewController.swift
//  CondorFilms
//
//  Created by Macbook  on 4/03/22.
//

import UIKit
import RxSwift
import RxCocoa
import RapidUtil

protocol IHomeViewController {
    var router: IHomeRouter? { get set }
    var viewModel: IHomeViewModel? { get set }
    var const :  ConstantString? {get set}
    var popUpsPresenter : IPopUpsPresenter? {get set}
}

class HomeViewController: BaseViewController {
    @IBOutlet weak var collectionMovie: UICollectionView!
    @IBOutlet weak var buttonFavorite: UIButton!
    
    var router: IHomeRouter?
    var viewModel: IHomeViewModel?
    var disposeBag = DisposeBag()
    var const :  ConstantString?
    var popUpsPresenter : IPopUpsPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupRx()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel?.movieRequest.accept(())
        self.showLoading()
    }
    
    func setupViews() {
            overrideUserInterfaceStyle = .light
            self.collectionMovie.register(MovieCollectionViewCell.getNib(),
                                          forCellWithReuseIdentifier: "MovieCollectionViewCell")
        }
    func setupRx() {
        viewModel?.errorRequest.asObservable().bind{ error in
            DispatchQueue.main.async {
                self.hideLoading()
                let err = error as NSError
                
                let title = "\(self.const?.returnString(key: .popUpCodeTitle, table: .details) ?? "" )\(err.code)"
                let description = self.const?.returnString(key: .popUpsHttpError, table: .details) ?? ""
                self.popUpsPresenter?.presentPopUps(view: self, popUps:NavigationMaps.PopUpsFlowNavigation.popUpFavorite(self,  title, description,true))
            }
            
        }.disposed(by: disposeBag)
        
        viewModel?.output?.response.asObservable().bind(to: collectionMovie.rx.items(cellIdentifier: "MovieCollectionViewCell", cellType: MovieCollectionViewCell.self)){index, element, cell  in
                cell.labelTitle.text = element.title
                cell.imagePoster.downloaded(from: "\(ApiParams.urlImage)\(element.poster_path ?? "")")
            cell.rating = CGFloat(Double(element.vote_average ?? 0))
            cell.imagePoster.contentMode = .scaleAspectFill
        }.disposed(by: disposeBag)
        
        viewModel?.output?.response.asObservable().subscribe(onNext:{_ in
           self.hideLoading()
        }).disposed(by: disposeBag)
        
        viewModel?.outputDetail?.response.asObservable().bind{ response in
            self.hideLoading()
            self.router?.navigationToDetail(detail:response)
        }.disposed(by: disposeBag)
        
        viewModel?.output?.errorMessage.asObservable().bind{ messaje in
            self.hideLoading()
        }.disposed(by: disposeBag)
        
        viewModel?.outputDetail?.errorMessage.asObservable().bind{ messaje in
            self.hideLoading()
        }.disposed(by: disposeBag)
        collectionMovie
                .rx
                .modelSelected(ResultMovie.self)
                .subscribe(onNext: { (model) in
                    self.viewModel?.movieDetailRequest.accept(model.id ?? 0)
                    self.showLoading()
                }).disposed(by: disposeBag)
        
        buttonFavorite.rx.tap.bind{
            self.hideLoading()
            self.router?.navigationToFavoritel()
        }.disposed(by: disposeBag)
    }
}

extension HomeViewController : IHomeViewController{

}

extension HomeViewController : IPopUps{
    func actionDone(isOneAction: Bool) {
        print("actionDone")
        popUpsPresenter?.dismissPopUps(viewcont: self)
    }
    
    func actionCancel() {
        print("actionDone")
    }
}
