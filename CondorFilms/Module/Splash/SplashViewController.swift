//
//  SplashViewController.swift
//  condorFilms
//
//  Created by Macbook  on 4/03/22.
//

import UIKit
import Lottie
import RxSwift

protocol ISplashViewController {
    var router: ISplashRouter? { get set }
    var lottieHelper : IHelperLottie? { get set }
}
class SplashViewController: BaseViewController , ISplashViewController{
    
    @IBOutlet weak var vwAnimation : UIView!
    
    var animationView = AnimationView()
    var router: ISplashRouter?
    var lottieHelper : IHelperLottie?
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startAnimation()
        confAnimation()
    }
    
    func confAnimation() {
        lottieHelper?.animation?.accept(("animationCondorFilms", "Animations", vwAnimation))
    }
    
    func startAnimation(){
        lottieHelper?.onAnimation?.asObservable().bind{ animation in
            animation.play(fromProgress: 0,
                                       toProgress: 1,
                                       loopMode: LottieLoopMode.playOnce,
                                       completion: { (finished) in
                                        if finished {
                                            self.router?.navigationHome()
                                        } else {
                                            print("Animation cancelled")
                                        }
                    })
        }.disposed(by: disposeBag)
    }
}

