//
//  SplashViewModel.swift
//  CondorFilms
//
//  Created by Macbook  on 4/03/22.
//

//import Foundation
//import RxSwift
//import RxRelay
//import RxCocoa
//import Lottie
//
//protocol ISplashViewModel {
//    var animation : PublishRelay<(String,String)>? { get set }
//    var view : Driver<AnimationView>? { get set }
//}
//
//class SplashViewModel : ISplashViewModel {
//    var animation: PublishRelay<(String, String)>?
//    var view : Driver<AnimationView>?
//
//    init(){
//        self.animation = PublishRelay<(String, String)>()
//        initAnimation()
//    }
//    func initAnimation(){
//        self.view  = self.animation?.flatMapLatest{ (archive,rute) -> Observable<AnimationView> in
//            return Observable.create{ (observer) -> Disposable in
//                guard let animation = Animation.named("animationCondorFilms", subdirectory: "Animations")
//                else {
//                    observer.onError(ErrorGeneric.noFound)
//                    return Disposables.create()
//                }
//                let animationView = AnimationView()
//                animationView.animation = animation
//                animationView.contentMode = .scaleAspectFit
//
//                observer.onNext(animationView)
//                return Disposables.create()
//            }
//        }.asDriver(onErrorRecover: { Error in
//            return Driver.just(AnimationView())
//        })
//    }
//}
