//
//  SplashRouter.swift
//  condorFilms
//
//  Created by Macbook  on 4/03/22.
//

import UIKit
import RapidUtil

protocol ISplashRouter : IRouter{
    func navigationHome()
}

class SplashRoute: ISplashRouter {
    weak var view: UIViewController?
    
    init(_ view : UIViewController){
        self.view = view
    }
    
    func navigationHome() {
        view?.presentView(NavigationMaps.principalFlowNavigation.homeViewController, styleModal: .fullScreen)
    }
}
