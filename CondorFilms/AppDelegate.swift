//
//  AppDelegate.swift
//  CondorFilms
//
//  Created by Macbook  on 4/03/22.
//

import UIKit
import RapidUtil

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let root = UINavigationController.init(rootViewController: NavigationMaps
                                                .principalFlowNavigation
                                                .splashViewController
                                                .getViewController())
        
        print(root)
        window?.rootViewController = root
        window?.makeKeyAndVisible()
        
        // Override point for customization after application launch.
        return true
    }
}

